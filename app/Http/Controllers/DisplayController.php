<?php

namespace App\Http\Controllers;

use App\Models\Stop;
use Illuminate\Http\Request;

class DisplayController extends Controller
{
    public function index()
    {
        return view('welcome')->with([
            'stops' => Stop::all(),
        ]);
    }
}
