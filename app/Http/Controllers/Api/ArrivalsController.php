<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArrivalTimeRequest;
use App\Library\DataMall;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ArrivalsController extends Controller
{
    public function index(ArrivalTimeRequest $request)
    {
        return response()->json([
            'data' => (new DataMall(config('barrivat.datamall_api_key')))
                ->getBusArrivalTime($request->input('stop_number'), $request->input('bus_number')),
        ]);
    }
}
