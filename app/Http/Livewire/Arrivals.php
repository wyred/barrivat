<?php

namespace App\Http\Livewire;

use App\Library\DataMall;
use Livewire\Component;

class Arrivals extends Component
{
    public $stop_number;
    public $bus_number;

    public $tta_1 = null;
    public $tta_2 = null;
    public $load_1 = 'text-transparent';
    public $load_2 = 'text-transparent';
    public $type_1 = '';
    public $type_2 = '';

    public function render()
    {
        return view('livewire.arrivals');
    }

    public function mount($stop_number, $bus_number)
    {
        $this->stop_number = $stop_number;
        $this->bus_number = $bus_number;
    }

    public function fetch()
    {
        $arrivals = (new DataMall(config('barrivat.datamall_api_key')))
            ->getBusArrivalTime($this->stop_number, $this->bus_number);

        $this->tta_1 = gmdate('i:s', $arrivals[0]['tta']);
        $this->tta_2 = gmdate('i:s', $arrivals[1]['tta']);

        $this->load_1 = $this->parseLoad($arrivals[0]['load']);
        $this->load_2 = $this->parseLoad($arrivals[1]['load']);

        $this->type_1 = $arrivals[0]['type'];
        $this->type_2 = $arrivals[1]['type'];
    }

    public function parseLoad($load_level)
    {
        switch ($load_level) {
            case 0:
                return 'text-green-300';
            case 5:
                return 'text-yellow-300';
            case 9:
                return 'text-red-400';
        }
    }
}
