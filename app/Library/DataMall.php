<?php

namespace App\Library;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class DataMall
{
    private $api_key = null;
    private $url = 'http://datamall2.mytransport.sg';

    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }

    public function getBusArrivalTime($stop, $bus)
    {
        $response = Http::withHeaders([
            'AccountKey' => $this->api_key,
        ])->get($this->url . '/ltaodataservice/BusArrivalv2', [
            'BusStopCode' => $stop,
            'ServiceNo' => $bus,
        ]);

        $data = $response->json('Services')[0];

        $remap_load = [
            'SEA' => 0,
            'SDA' => 5,
            'LSD' => 9,
        ];

        $remap_type = [
            'SD' => 1,
            'DD' => 2,
            'BD' => 1,
        ];

        $results = [];
        foreach (['NextBus', 'NextBus2', 'NextBus3'] as $key) {
            if (empty($data[$key]['EstimatedArrival'])) {
                continue;
            }

            $results[] = [
                'tta' => Carbon::parse($data[$key]['EstimatedArrival'])->diffInSeconds(now()),
                'load' => $remap_load[$data[$key]['Load']],
                'type' => $data[$key]['Type'],
            ];
        }

        return $results;
    }
}