<x-guest-layout>
    @foreach ($stops as $stop)
        <div class="mb-5 px-3 py-2 bg-white rounded-lg bg-opacity-25">
            <span class="text-sm font-semibold uppercase">{{ $stop->number }} - {{ $stop->description }}</span>
            @foreach ($stop->buses as $bus)
                @livewire('arrivals', ['stop_number' => $stop->number, 'bus_number' => $bus->number])
            @endforeach
        </div>
    @endforeach
</x-guest-layout>
