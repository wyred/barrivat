<div>
    <div class="grid grid-cols-3">
        <div class="text-4xl font-light leading-tight" wire:click="fetch()">{{ $bus_number }}</div>
        <div class="text-right">
            <div class="text-2xl font-light leading-none">{{ $tta_1 }}</div>
            <div class="text-sm leading-none">
                <span class="text-xs font-bold mr-1">{{ $type_1 }}</span>
                <svg xmlns="http://www.w3.org/2000/svg" class="inline-block {{ $load_1 }} fill-current h-4 mr-1" viewBox="0 0 192 512"><path d="M96 0c35.346 0 64 28.654 64 64s-28.654 64-64 64-64-28.654-64-64S60.654 0 96 0m48 144h-11.36c-22.711 10.443-49.59 10.894-73.28 0H48c-26.51 0-48 21.49-48 48v136c0 13.255 10.745 24 24 24h16v136c0 13.255 10.745 24 24 24h64c13.255 0 24-10.745 24-24V352h16c13.255 0 24-10.745 24-24V192c0-26.51-21.49-48-48-48z"/></svg>
            </div>
        </div>
        <div class="text-right">
            <div class="text-2xl leading-none">{{ $tta_2 }}</div>
            <div class="text-sm leading-none">
                <span class="text-xs font-bold mr-1">{{ $type_2 }}</span>
                <svg xmlns="http://www.w3.org/2000/svg" class="inline-block {{ $load_2 }} fill-current h-4 mr-1" viewBox="0 0 192 512"><path d="M96 0c35.346 0 64 28.654 64 64s-28.654 64-64 64-64-28.654-64-64S60.654 0 96 0m48 144h-11.36c-22.711 10.443-49.59 10.894-73.28 0H48c-26.51 0-48 21.49-48 48v136c0 13.255 10.745 24 24 24h16v136c0 13.255 10.745 24 24 24h64c13.255 0 24-10.745 24-24V352h16c13.255 0 24-10.745 24-24V192c0-26.51-21.49-48-48-48z"/></svg>
            </div>
        </div>
    </div>
</div>
